package at.campus02.nowa.consolein;

import java.io.IOException;

public class ConsoleInApp {

	public static void main(String[] args) {
		
		int byteRead = 0;
		
		try {
			while((byteRead = System.in.read()) != -1 ){
				char[] b = Character.toChars(byteRead);
				
				if (b[0] == 'x') {
					System.exit(0);
				}
				System.out.print(b[0]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
